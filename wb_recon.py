import argparse
from os import path
from pathlib import PurePath
from openpyxl import load_workbook
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument('file1', help="The full file path to the Water Balances Spreadsheet")
parser.add_argument('file2', help="The full file path to the Ledger Balances Spreadsheet")

COLUMNS_TO_KEEP = [60, 46, 44, 43, 37, 36, 35, 33, 31, 27, 25, 24, 22, 17, 16, 15, 12, 11, 10, 8, 9, 4, 3, 2]


def reconcile_water_balances(wbs_path, t1s_path):

    if not path.exists(wbs_path):
        raise FileNotFoundError(str.format('{} is not a valid file', wbs_path))

    if not path.exists(t1s_path):
        raise FileNotFoundError(str.format('{} is not a valid file', t1s_path))

    # Establish the name of the cleaned wbs file
    dest_filename = wbs_path.name[0: len(wbs_path.name) - 4] + 'xlsx'
    dest_path = PurePath(wbs_path.with_name(dest_filename))

    # Create Cleaned wsb file
    print(str.format('Cleaning up {}', wbs_path.name))
    clean_wbs(wbs_path, dest_path)

    # Load excel files into Dataframes
    print('Loading files into dataframes....')
    wbs = pd.read_excel(dest_path, sheet_name='Reconciliation', index_col=0)
    t1s = pd.read_excel(t1s_path, sheet_name='Sheet1', index_col=0)

    # run the reconciliation between the two data sets
    rec_worksheet = calculate_variances(t1s, wbs)

    # save the calculated variances to a reconciliation file
    rec_worksheet.to_excel(dest_path.with_name('water_balances_rec.xlsx'), 'recon')


def calculate_variances(df1, df2):

    # merge the two data frames
    df = pd.merge(df1, df2, how='outer', left_index=True, right_index=True)

    # drop the columns that are not required
    drop_cols = [col for col in df.columns.values if "Unnamed" in col]
    drop_cols = drop_cols + ['wb_swaa_id', 'wb_notes']
    df.drop(columns=drop_cols, inplace=True)

    # Replace all the NaN values with 0
    df.fillna(0, inplace=True)

    # GS Entitlement Variance
    df['c1_ent_var'] = df['t1_c1_ent'] - df['wb_c1_ent']
    df['c1_lease_var'] = df['t1_c1_lease'] - df['wb_c1_lease']
    # HS Entitlement Variance
    df['c3_ent_var'] = df['t1_c3_ent'] - df['wb_c3_ent']
    df['c3_lease_var'] = df['t1_c3_lease'] - df['wb_c3_lease']
    # S&D Entitlement Variance
    df['c7_ent_var'] = df['t1_c7_ent'] - df['wb_c7_ent']
    # Delivery Entitlements Variance
    df['de_ent_var'] = df['t1_de_base'] - df['wb_de_base']
    df['de_move_var'] = df['t1_de_traded'] - df['wb_de_terminated'] - df['wb_de_traded']
    # Current Year Allocation Variance
    df['allocation_var'] = df['t1_allocation'] - df['wb_c1_allocation'] - df['wb_c3_allocation'] \
        - df['wb_c7_allocation']
    df['enhancement_var'] = df['t1_enhancement'] - df['wb_enhancement']
    # Prior Year Carryover
    df['py_cf_var'] = df['t1_py_cf'] - df['wb_py_cf']
    # Prior Year Overuse
    df['py_overuse_var'] = df['t1_py_overuse'] - df['wb_py_overuse']
    # Temporary Trade Variance
    df['trade_var'] = df['t1_allocation_trade'] - df['wb_internal_trade'] - df['wb_c1_external_trade'] \
        - df['wb_c3_external_trade']
    # Metered Usage Variance
    df['metered_usage_var'] = df['t1_metered_usage'] + df['wb_metered_usage']
    # Unmetered Usage Variance
    df['unmetered_usage_var'] = df['t1_unmetered_usage'] + df['wb_unmetered_usage']
    # Remaining Allocation Var
    df['remaining_allocation_var'] = df['t1_remaining_allocation'] - df['wb_remaining_allocation']
    # Current Year Carry forward Var
    df['cy_cf_var'] = df['t1_cy_cf'] - df['wb_cy_cf']

    return df


def clean_wbs(wbs_path, dest_path):
    """
    :param dest_path: path to the reconciliation file
    :param wbs_path: path to the workbook to be loaded
    :return: pandas dataframe
    """

    print(str('Loading Workbook - {}').format(wbs_path.name))
    wb = load_workbook(wbs_path, data_only=True)

    for name in wb.sheetnames:
        ws = wb[name]
        if name == 'WAAs':
            print('Copying the WAAs worksheet.....')
            recon_ws = wb.copy_worksheet(ws)
            recon_ws.title = 'Reconciliation'
            clean_up_water_balances_ws(recon_ws)

            # Delete the original WAAs sheet
            wb.remove(ws)
        else:
            # Delete the sheet, not required for reconciliation
            print("Deleting Sheet {}".format(name))
            wb.remove(ws)

    print('Saving updated file')
    wb.save(dest_path)
    wb.close()

    print('Complete')


def clean_up_water_balances_ws(worksheet):
    """
    :param worksheet: an openpyxl worksheet
    """

    print('Removing unused columns .....')
    # Delete Columns that are not required - Work from highest to lowest
    for i in range(worksheet.max_column, 0, -1):

        if i not in COLUMNS_TO_KEEP:
            print(str.format('Deleting column {}', i))
            worksheet.delete_cols(i)

    print('Adding the Header Row .....')
    # Insert Header Row
    worksheet.insert_rows(1)
    worksheet['A1'] = 'wb_allocation_id'
    worksheet['B1'] = 'wb_swaa_id'
    worksheet['C1'] = 'wb_notes'
    worksheet['D1'] = 'wb_c1_ent'
    worksheet['E1'] = 'wb_c1_lease'
    worksheet['F1'] = 'wb_c3_ent'
    worksheet['G1'] = 'wb_c7_ent'
    worksheet['H1'] = 'wb_c3_lease'
    worksheet['I1'] = 'wb_de_base'
    worksheet['J1'] = 'wb_de_terminated'
    worksheet['K1'] = 'wb_de_traded'
    worksheet['L1'] = 'wb_c1_allocation'
    worksheet['M1'] = 'wb_enhancement'
    worksheet['N1'] = 'wb_c3_allocation'
    worksheet['O1'] = 'wb_c7_allocation'
    worksheet['P1'] = 'wb_py_cf'
    worksheet['Q1'] = 'wb_py_overuse'
    worksheet['R1'] = 'wb_internal_trade'
    worksheet['S1'] = 'wb_c1_external_trade'
    worksheet['T1'] = 'wb_c3_external_trade'
    worksheet['U1'] = 'wb_unmetered_usage'
    worksheet['V1'] = 'wb_metered_usage'
    worksheet['W1'] = 'wb_remaining_allocation'
    worksheet['X1'] = 'wb_cy_cf'

    print('Deleting unused rows .....')
    # Delete Redundant Headers
    worksheet.delete_rows(2, 6)

    # Iterate over the rows and remove invalid columns
    for i in range(worksheet.max_row, 2, -1):

        row = worksheet[i]
        if not is_valid(row):
            print(str.format('Deleting row {}', row[0].row))
            worksheet.delete_rows(i)


def is_valid(row):

    # print(row[0].value, row[1].value, row[2].value, sep=',')
    ret_value = True

    if row[2].value == 'SWAA':
        print('*** Invalid - SWAA Total')
        ret_value = False
    elif row[0].value is None:
        print('*** Invalid - Blank Allocation Id')
        ret_value = False
    elif row[0].value == 9:
        print('*** Invalid - In Transit')
        ret_value = False

    return ret_value


if __name__ == '__main__':
    # reconcile_water_balances()

    args = parser.parse_args()
    reconcile_water_balances(PurePath(args.file1), PurePath(args.file2))
